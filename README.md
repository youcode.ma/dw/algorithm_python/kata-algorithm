
# Exercices d'application algorithm avec python
source:  http://pise.info/algo/codage.htm 

# Thème 1 - Logique et tests

## Exercice 1.1
Les habitants de Zorglub paient l’impôt selon les règles suivantes :  
* les hommes de plus de 20 ans paient l’impôt  
* les femmes paient l’impôt si elles ont entre 18 et 35 ans  
* les autres ne paient pas d’impôt  

Le programme demandera donc l’âge et le sexe du Zorglubien, et se prononcera donc ensuite sur le fait que l’habitant est imposable.  

## Exercice 1.2
Les élections législatives, en Guignolerie Septentrionale, obéissent à la règle suivante :  

* lorsque l'un des candidats obtient plus de 50% des suffrages, il est élu dès le premier tour.  
* en cas de deuxième tour, peuvent participer uniquement les candidats ayant obtenu au moins 12,5% des voix au premier tour.  

Vous devez écrire un algorithme qui permette la saisie des scores de quatre candidats au premier tour. Cet algorithme traitera ensuite le candidat numéro 1 (et uniquement lui) : il dira s'il est élu, battu, s'il se trouve en ballottage favorable (il participe au second tour en étant arrivé en tête à l'issue du premier tour) ou défavorable (il participe au second tour sans avoir été en tête au premier tour).  

## Exercice 1.3
Une compagnie d'assurance automobile propose à ses clients quatre familles de tarifs identifiables par une couleur, du moins au plus onéreux : tarifs bleu, vert, orange et rouge. Le tarif dépend de la situation du conducteur :  

 * un conducteur de moins de 25 ans et titulaire du permis depuis moins de deux ans, se voit attribuer le tarif rouge, si toutefois il n'a jamais été responsable d'accident. Sinon, la compagnie refuse de l'assurer.  
 * un conducteur de moins de 25 ans et titulaire du permis depuis plus de deux ans, ou de plus de 25 ans mais titulaire du permis depuis moins de deux ans a le droit au tarif orange s'il n'a jamais provoqué d'accident, au tarif rouge pour un accident, sinon il est refusé.  
 * un conducteur de plus de 25 ans titulaire du permis depuis plus de deux ans bénéficie du tarif vert s'il n'est à l'origine d'aucun accident et du tarif orange pour un accident, du tarif rouge pour deux accidents, et refusé au-delà  
 De plus, pour encourager la fidélité des clients acceptés, la compagnie propose un contrat de la couleur immédiatement la plus avantageuse s'il est entré  dans la maison depuis plus de cinq ans. Ainsi, s'il satisfait à cette exigence, un client normalement "vert" devient "bleu", un client normalement "orange"  devient "vert", et le "rouge" devient orange.  

Ecrire l'algorithme permettant de saisir les données nécessaires (sans contrôle de saisie) et de traiter ce problème. Avant de se lancer à corps perdu dans cet  exercice, on pourra réfléchir un peu et s'apercevoir qu'il est plus simple qu'il n'en a l'air (cela s'appelle faire une analyse !)

## Exercice 1.4
Ecrivez un algorithme qui a près avoir demandé un numéro de jour, de mois et d'année à l'utilisateur, renvoie s'il s'agit ou non d'une date valide.  
Cet exercice est certes d’un manque d’originalité affligeant, mais après tout, en algorithmique comme ailleurs, il faut connaître ses classiques ! Et quand on a  fait cela une fois dans sa vie, on apprécie pleinement l’existence d’un type numérique « date » dans certains langages…).  
Il n'est sans doute pas inutile de rappeler rapidement que le mois de février compte 28 jours, sauf si l’année est bissextile, auquel cas il en compte 29.  L’année est bissextile si elle est divisible par quatre. Toutefois, les années divisibles par 100 ne sont pas bissextiles, mais les années divisibles par 400 le sont. Ouf !  


# Thème 2 - Boucles

## Exercice 2.1
Ecrire un algorithme qui demande un nombre de départ, et qui ensuite affiche les dix nombres suivants. Par exemple, si l'utilisateur entre le nombre 17, le programme affichera les nombres de 18 à 27.  

## Exercice 2.2
Réécrire l'algorithme précédent, en utilisant cette fois l'instruction Pour  

## Exercice 2.3
Ecrire un algorithme qui demande un nombre de départ, et qui ensuite écrit la table de multiplication de ce nombre, présentée comme suit (cas où l'utilisateur entre le nombre 7) :  
Table de 7 :  
7 x 1 = 7  
7 x 2 = 14  
7 x 3 = 21  
…  
7 x 10 = 70  

## Exercice 2.4
Ecrire un algorithme qui demande un nombre de départ, et qui calcule la somme des entiers jusqu’à ce nombre. Par exemple, si l’on entre 5, le programme doit calculer :  
1 + 2 + 3 + 4 + 5 = 15  
NB : on souhaite afficher uniquement le résultat, pas la décomposition du calcul.  

## Exercice 2.5
Ecrire un algorithme qui demande un nombre de départ, et qui calcule sa factorielle.  
NB : la factorielle de 8, notée 8 !, vaut 1 x 2 x 3 x 4 x 5 x 6 x 7 x 8  

## Exercice 2.6
Ecrire un algorithme qui demande successivement 20 nombres à l’utilisateur, et qui lui dise ensuite quel était le plus grand parmi ces 20 nombres :  
Entrez le nombre numéro 1 : 12  
Entrez le nombre numéro 2 : 14  
etc.  
Entrez le nombre numéro 20 : 6  
Le plus grand de ces nombres est  : 14  
Modifiez ensuite l’algorithme pour que le programme affiche de surcroît en quelle position avait été saisie ce nombre :  
C’était le nombre numéro 2 

## Exercice 2.7
Réécrire l’algorithme précédent, mais cette fois-ci on ne connaît pas d’avance combien l’utilisateur souhaite saisir de nombres. La saisie des nombres s’arrête lorsque l’utilisateur entre un zéro.  


# Thème 3 - Les tableaux

## Exercice 3.1
* Ecrire un algorithme qui déclare et remplisse un tableau de 7 valeurs numériques en les mettant toutes à zéro.
* Ecrire un algorithme qui déclare et remplisse un tableau contenant les six voyelles de l’alphabet latin.
* Ecrire un algorithme qui déclare un tableau de 9 notes, dont on fait saisir les valeurs par l’utilisateur.

## Exercice 3.2
Ecrivez un algorithme permettant à l’utilisateur de saisir un nombre quelconque de valeurs, qui devront être stockées dans un tableau. 
L’utilisateur doit donc commencer par entrer le nombre de valeurs qu’il compte saisir. Il effectuera ensuite cette saisie. Enfin, une fois la saisie terminée, le programme affichera le nombre de valeurs négatives et le nombre de valeurs positives.  

## Exercice 3.3
Ecrivez un algorithme calculant la somme des valeurs d’un tableau (on suppose que le tableau a été préalablement saisi).

## Exercice 3.4
Ecrivez un algorithme constituant un tableau, à partir de deux tableaux de même longueur préalablement saisis. Le nouveau tableau sera la somme des éléments des deux tableaux de départ.  
Tableau 1 :  
4   8   7   9   1   5   4   6  

Tableau 2 :  
7   6   5   2   1   3   7   4  

Tableau à constituer :  
11  14  12  11  2   8   11  10  


## Exercice 3.5
Toujours à partir de deux tableaux précédemment saisis, écrivez un algorithme qui calcule le schtroumpf des deux tableaux. Pour calculer le schtroumpf, il faut multiplier chaque élément du tableau 1 par chaque élément du tableau 2, et additionner le tout. Par exemple si l'on a :  
Tableau 1 :  
4   8   7   12  

Tableau 2 :  
3   6  

Le Schtroumpf sera :  
3 * 4 + 3 * 8 + 3 * 7 + 3 * 12 + 6 * 4 + 6 * 8 + 6 * 7 + 6 * 12 = 279  

## Exercice 3.6
Écrivez un algorithme remplissant un tableau à deux dimensions de 6 sur 13, avec des zéros.  

## Exercice 3.7
Soit un tableau T à deux dimensions (12, 8) préalablement rempli de valeurs numériques.  
Écrire un algorithme qui recherche la plus grande valeur au sein de ce tableau. 

# Thème 4 - Tri et recherche

##  Exercice 4.1
Ecrivez un algorithme qui permette de saisir un nombre quelconque de valeurs, et qui les range au fur et à mesure dans un tableau. Le programme, une fois la saisie terminée, doit dire si les éléments du tableau sont tous consécutifs ou non.
Par exemple, si le tableau est :
 
12  13  14  15  16  17  18

ses éléments sont tous consécutifs. En revanche, si le tableau est :
 
9   10  11  15  16  17  18

ses éléments ne sont pas tous consécutifs.

## Exercice 4.2
Ecrivez un algorithme qui trie un tableau dans l’ordre décroissant.
Vous écrirez bien entendu deux versions de cet algorithme, l'une employant le tri par sélection, l'autre le tri à bulles.

## Exercice 4.3
Ecrivez un algorithme qui inverse l’ordre des éléments d’un tableau dont on suppose qu'il a été préalablement saisi (« les premiers seront les derniers… »)

## Exercice 4.4
Ecrivez un algorithme qui permette à l’utilisateur de supprimer une valeur d’un tableau préalablement saisi. L’utilisateur donnera l’indice de la valeur qu’il souhaite supprimer. Attention, il ne s’agit pas de remettre une valeur à zéro, mais bel et bien de la supprimer du tableau lui-même ! Si le tableau de départ était :
 
12  8   4   45  64  9   2

Et que l’utilisateur souhaite supprimer la valeur d’indice 4, le nouveau tableau sera :
 
12  8   4   45  9   2

## Exercice 4.5
Ecrivez l'algorithme qui recherche un mot saisi au clavier dans un dictionnaire. Le dictionnaire est supposé être codé dans un tableau préalablement rempli et trié.

## Exercice 4.6
Écrivez un algorithme qui fusionne deux tableaux (déjà existants) dans un troisième, qui devra être trié.
Attention ! On présume que les deux tableaux de départ sont préalablement triés : il est donc irrationnel de faire une simple concaténation des deux tableaux de départ, puis d'opérer un tri : comme quand on se trouve face à deux tas de papiers déjà triés et qu'on veut les réunir, il existe une méthode bien plus économique (et donc, bien plus rationnelle...) 


# Thème 5 - Procédure et fonctions

## Exercice 5.1
Écrivez une fonction qui renvoie la somme de cinq nombres fournis en argument.  

## Exercice 5.2
Écrivez une fonction qui renvoie le nombre de voyelles contenues dans une chaîne de caractères passée en argument. Au passage, notez qu'une fonction a tout à fait le droit d'appeler une autre fonction.  

## Exercice 5.3
Ecrivez une fonction qui purge une chaîne d'un caractère, la chaîne comme le caractère étant passés en argument. Si le caractère spécifié ne fait pas partie de la chaîne, celle-ci devra être retournée intacte.  
Par exemple :  

    Purge("Bonjour","o") renverra "Bnjur"
    Purge("J'ai horreur des espaces"," ") renverra "J'aihorreurdesespaces"
    Purge("Moi, je m'en fous", "y") renverra "Moi, je m'en fous"

## Exercice 5.4
Même question que précédement, mais cette fois, on doit pouvoir fournir un nombre quelconque de caractères à supprimer en argument.  

## Exercice 5.5
Ecrire un traitement qui effectue le tri d'un tableau envoyé en argument (on considère que le code appelant devra également fournir le nombre d'éléments du tableau).  

## Exercice 5.6
Ecrire un traitement qui informe si un un tableau envoyé en argument est formé ou non d'éléments tous rangés en ordre croissant.  

## Exercice 5.7
Ecrire un traitement qui inverse le contenu de deux valeurs passées en argument.  

## Exercice 5.8
reprendre l'exercice 5, mais cette fois la procédure comprendra un troisième paramètre, de type booléen. VRAI, celui-ci indiquera que le tri devra être effectué dans l'ordre croissant, FAUX dans l'ordre décroissant.  

# Thème 6 - Fichier

## Exercice 6.1
Ecrivez un algorithme qui permet d'ouvrir un fichier et afficher son contenu

## Exercice 6.2
On travaille avec le fichier du carnet d’adresses en champs de largeur fixe.  
Ecrivez un algorithme qui permet à l’utilisateur de saisir au clavier un nouvel individu qui sera ajouté à ce carnet d’adresses.  

## Exercice 6.3
Même question, mais cette fois le carnet est supposé être trié par ordre alphabétique. L’individu doit donc être inséré au bon endroit dans le fichier.  

## Exercice 6.4
Ecrivez un algorithme qui permette de modifier un renseignement (pour simplifier, disons uniquement le nom de famille) d’un membre du carnet d’adresses. Il faut donc demander à l’utilisateur quel est le nom à modifier, puis quel est le nouveau nom, et mettre à jour le fichier. Si le nom recherché n'existe pas, le programme devra le signaler.  

## Exercice 6.5
Ecrivez un algorithme qui trie les individus du carnet d’adresses par ordre alphabétique.  

## Exercice 6.6
Soient Toto.txt et Tata.txt deux fichiers dont les enregistrements ont la même structure. Ecrire un algorithme qui recopie tout le fichier Toto dans le fichier Tutu, puis à sa suite, tout le fichier Tata (concaténation de fichiers).  

## Exercice 6.7
Ecrire un algorithme qui supprime dans notre carnet d'adresses tous les individus dont le mail est invalide (pour employer un critère simple, on considèrera que sont invalides les mails ne comportant aucune arobase, ou plus d'une arobase).  

## Exercice 6.8
Les enregistrements d’un fichier contiennent les deux champs Nom (chaîne de caractères) et Montant (Entier). Chaque enregistrement correspond à une vente conclue par un commercial d’une société.  
On veut mémoriser dans un tableau, puis afficher à l'écran, le total de ventes par vendeur. Pour simplifier, on suppose que le fichier de départ est déjà trié alphabétiquement par vendeur.  
